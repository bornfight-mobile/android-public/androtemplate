package com.bornfight.appname.firebase

import android.app.PendingIntent
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.net.Uri
import android.util.Log
import android.util.TypedValue
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.bornfight.appname.App
import com.bornfight.appname.BuildConfig
import com.bornfight.appname.R
import com.bornfight.appname.main.MainActivity
import com.bornfight.common.dagger.components.DaggerServiceComponent
import com.bornfight.common.firebase.NotificationChannelsHelper
import com.bornfight.common.firebase.ProcessOpenNotificationService
import com.bornfight.common.session.DevicePreferences
import com.bornfight.common.session.Session
import com.bornfight.common.session.SessionPrefImpl
import com.bumptech.glide.Glide
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import java.util.concurrent.ExecutionException
import javax.inject.Inject

/**
 * Created by tomislav on 27/01/2017.
 */

class MyFirebaseMessagingService : FirebaseMessagingService() {
    private var mGson: Gson = Gson()

    @Inject
    lateinit var mSessionPref: SessionPrefImpl

    @Inject
    lateinit var mDevicePref: DevicePreferences

    override fun onCreate() {
        super.onCreate()

        DaggerServiceComponent.builder()
            .applicationComponent((application as App).component)
            .build()
            .inject(this)
    }

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Log.d(TAG, "Refreshed token: $s")

        // New token was generated and it needs to be sent to the server side
        mDevicePref.isFirebaseTokenSent = false
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        Log.d(TAG, "Notification data: " + remoteMessage.data)

        val data = remoteMessage.data

        //check if notification should be shown
        // TODO remove or keep the session flag check, depending if the app supports login/registration
        if (!checkRules(data) || !mSessionPref.isLoggedIn) return

        var type: String? = data["type"]
        if (type == null) type = "general"
        var action: String? = data["action"]
        if (action == null) action = ""

        if (!checkNotificationSettings(type, action, mSessionPref)) return

        when (type) {
            "update" -> processUpdateNotification(data)
            else -> processGeneralNotification(data)
        }
    }

    private fun checkRules(data: Map<String, String>): Boolean {
        val rule = if (data.containsKey("rule")) data["rule"] else ">"
        val appName = if (data.containsKey("appId")) data["appId"] else null
        val version = Integer.parseInt((data["version"] ?: "0"))

        val userId = java.lang.Long.parseLong(data["userId"] ?: "0")

        val versionRule = (rule == "=" && BuildConfig.VERSION_CODE == version
            || rule == "<" && BuildConfig.VERSION_CODE < version
            || rule == ">" && BuildConfig.VERSION_CODE > version)

        val appNameRule = appName == null || appName.equals(BuildConfig.APPLICATION_ID, ignoreCase = true)

        val userIdRule = userId == 0L || userId == mSessionPref.userId

        return versionRule && appNameRule && userIdRule
    }

    private fun checkNotificationSettings(type: String, action: String, session: Session): Boolean {
        // not being used at the moment
        //if (!session.isNotificationEnabled(Session.Notifications.ALL)) return false;

        return true
    }

    private fun processUpdateNotification(data: Map<String, String>) {
        val title = data["title"]
        val message = data["text"]
        val imageUrl = data["attachmentUrl"]

        val playStore = Intent(Intent.ACTION_VIEW,
            Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID))
        val pi = PendingIntent.getActivity(this, 0, playStore, PendingIntent.FLAG_UPDATE_CURRENT)

        showNotification(
            title,
            message,
            "updates",
            NotificationChannelsHelper.Channel.general,
            UPDATE_NOTIF_ID,
            pi,
            imageUrl, null,
            data
        )
    }

    private fun processGeneralNotification(data: Map<String, String>) {
        val title = data["title"]
        val message = data["text"]
        val imageUrl = data["attachmentUrl"]

        val pi = PendingIntent.getActivity(this, 0, Intent(this, MainActivity::class.java), PendingIntent.FLAG_UPDATE_CURRENT)
        showNotification(
            title,
            message,
            "general",
            NotificationChannelsHelper.Channel.general,
            0,
            pi,
            imageUrl, null,
            data
        )
    }


    private fun showNotification(title: String?,
                                 text: String?,
                                 groupKey: String?,
                                 channel: NotificationChannelsHelper.Channel,
                                 notificationId: Int,
                                 pendingIntent: PendingIntent,
                                 image: String?,
                                 actions: List<NotificationCompat.Action>?,
                                 notifData: Map<String, String>
    ) {

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channel.name)
            .setSmallIcon(R.mipmap.ic_notification)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
            .setContentTitle(title)
            .setContentText(text)
            .setStyle(NotificationCompat.BigTextStyle().bigText(text).setBigContentTitle(title))
            .setAutoCancel(true)
            .setColor(ContextCompat.getColor(this, R.color.colorAccent))
            .setLights(ContextCompat.getColor(this, R.color.colorAccent), 2000, 2000)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setSound(defaultSoundUri)
            .setContentIntent(buildOpenServiceIntent(
                pendingIntent,
                notificationId,
                notifData))
            .setDeleteIntent(buildDismissServiceIntent(
                notificationId,
                notifData))

        groupKey?.let {
            notificationBuilder.setGroup(groupKey)
        }

        actions?.forEach {
            notificationBuilder.addAction(it)
        }

        image?.let {
            val imageSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80f, resources.displayMetrics).toInt()
            try {
                notificationBuilder.setLargeIcon(Glide.with(this).asBitmap().load(it).submit(imageSize, imageSize).get())
            } catch (e: InterruptedException) {
                e.printStackTrace()
            } catch (e: ExecutionException) {
                e.printStackTrace()
            }
        }

        val notificationManager = NotificationManagerCompat.from(this)
        notificationManager.notify(notificationId, notificationBuilder.build())
    }

    private fun buildOpenServiceIntent(pendingIntent: PendingIntent, notificationId: Int, notifData: Map<String, String>): PendingIntent {
        return PendingIntent.getService(
            this,
            notificationId,
            ProcessOpenNotificationService.buildPendingOpenIntent(
                this,
                pendingIntent,
                notifData
            ), PendingIntent.FLAG_UPDATE_CURRENT)
    }

    private fun buildDismissServiceIntent(notificationId: Int, notifData: Map<String, String>): PendingIntent {
        return PendingIntent.getService(
            this,
            -notificationId,
            ProcessOpenNotificationService.buildDismissIntent(
                this,
                notifData
            ), PendingIntent.FLAG_UPDATE_CURRENT)
    }

    companion object {
        private val TAG = "FirebaseMsgService"
        private val UPDATE_NOTIF_ID = -1
    }
}

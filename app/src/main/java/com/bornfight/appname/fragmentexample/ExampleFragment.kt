package com.bornfight.appname.fragmentexample


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bornfight.appname.R
import com.bornfight.common.mvp.ui.BaseFragment
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 * Use the [ExampleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ExampleFragment : BaseFragment(), ExampleFragmentContract.View {

    @Inject
    lateinit var presenter: ExampleFragmentContract.Presenter<ExampleFragmentContract.View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent.inject(this)

    }

    override fun onResume() {
        super.onResume()
        presenter.subscribe(this)
        presenter.getDummy()
    }

    override fun onPause() {
        super.onPause()
        presenter.unsubscribe()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_example, container, false)

        return rootView
    }

    override fun showDummy(dummy: String) {
        showError(dummy)
    }

    companion object {

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment CribFragment.
         */
        fun newInstance(): ExampleFragment {
            val fragment = ExampleFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor

package com.bornfight.appname.fragmentexample

import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.mvp.BasePresenter
import com.bornfight.common.session.SessionPrefImpl
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by tomislav on 17/02/2017.
 */

class ExampleFragmentPresenter @Inject
constructor(compositeSubscription: CompositeDisposable, sessionPref: SessionPrefImpl,
            apiInterface: ApiInterface) : BasePresenter<ExampleFragmentContract.View>(compositeSubscription, sessionPref, apiInterface), ExampleFragmentContract.Presenter<ExampleFragmentContract.View> {

    private var dummyShown = false


    override fun getDummy() {
        if (!dummyShown) {
            view.showDummy("Hello from Fragment Presenter!")
        }
        dummyShown = true
    }
}

package com.bornfight.common.location

import android.Manifest
import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import com.bornfight.appname.R
import com.bornfight.common.mvp.ui.BaseActivity
import com.bornfight.utils.AppSettingsUtil
import com.bornfight.utils.DialogUtil
import com.greysonparrelli.permiso.Permiso

/**
 * Created by tomislav on 09/03/2017.
 * Updated by ianic on 06/08/2019.
 */

/**
 * Abstract class for location handling.
 * Extend this class and request location with requestLocation() method.
 * The class will automatically receive a location from [LocationLifecycleObserver]
 * in the [LocationListener] method implementations, depending on the
 * lifecycle status of this class.

 * It will automatically ask user for permission if non are given.
 */
abstract class LocationActivity : BaseActivity(), LocationListener {

    override val mContext: Context
        get() = this@LocationActivity

    private lateinit var mLocationObserver: LocationLifecycleObserver
    protected abstract val locationPermissionRationale: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mLocationObserver = LocationLifecycleObserver(this, lifecycle, session)
        lifecycle.addObserver(mLocationObserver)
    }

    /**
     * Use this method to explicitly request location which will be
     * available in the [LocationListener] method implementations
     */
    protected fun requestLocation(fine: Boolean, forceToAppSetings: Boolean) {
        val permissions = if (fine)
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
        else
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION)

        if (forceToAppSetings && permissions.fold(false) { initial, permission ->
                initial || checkPermissionDeniedForGood(permission) //at least one is denied
            }) {
            showAppSettingsDialog()
        } else if (checkPermissionsGranted(*permissions)) {
            mLocationObserver.getLocation()
        } else {
            showPermisoDialog(permissions)
        }
    }


    private fun showPermisoDialog(permissions: Array<String>) {
        Permiso.getInstance().requestPermissions(object : Permiso.IOnPermissionResult {
            override fun onPermissionResult(resultSet: Permiso.ResultSet) {
                if (resultSet.areAllPermissionsGranted()) {
                    onLocationPermissionGranted(true)
                    mLocationObserver.getLocation()
                } else {
                    onLocationPermissionGranted(false)
                }
            }

            override fun onRationaleRequested(callback: Permiso.IOnRationaleProvided, vararg permissions: String) {
                Permiso.getInstance().showRationaleInDialog(
                    getString(R.string.enable_location),
                    locationPermissionRationale, null,
                    callback
                )
            }
        }, *permissions)
    }

    protected fun checkLocationServicesEnabled(): Boolean {
        (getSystemService(Context.LOCATION_SERVICE) as LocationManager).let {
            return it.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                it.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        }
    }


    private fun showAppSettingsDialog() {
        DialogUtil.buildConfirmationDialog(
            this,
            getString(R.string.enable_location),
            getString(R.string.check_app_settings_for_location_permission), null, null,
            object : DialogUtil.OnConfirmationDialogClickListener {
                override fun onConfirm() {
                    AppSettingsUtil.startAppSettings(this@LocationActivity)
                }

                override fun onCancel() {

                }
            }
        ).show()
    }

    abstract override fun onLocationReady(lastKnownLocation: Location)

    abstract override fun onLocationUnavailable()

    /**
     * Notifies if the permission was granted.
     * Use for UI specific actions (hiding request location button etc.)
     *
     * @param granted if permission was granted or not
     */
    protected abstract fun onLocationPermissionGranted(granted: Boolean)
}

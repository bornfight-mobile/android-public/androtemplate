package com.bornfight.common.mvp


import android.util.Log
import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.data.retrofit.RetrofitException
import com.bornfight.common.session.SessionPrefImpl
import com.crashlytics.android.Crashlytics
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * Created by tomislav on 25/02/16.
 */
open class BasePresenter<TView : BaseContract.View> @Inject
constructor(protected val compositeDisposable: CompositeDisposable,
            protected val session: SessionPrefImpl,
            protected val apiInterface: ApiInterface) : BaseContract.Presenter<TView> {

    private lateinit var mView: WeakReference<TView>

    protected val view: TView
        get() {
            return mView.get() ?: throw MvpViewNotAttachedException()
        }

    override fun subscribe(view: TView) {
        mView = WeakReference(view)
    }

    override fun unsubscribe() {
        compositeDisposable.clear()
        mView.clear()
    }

    override fun handleError(e: Throwable) {
        handleError(e, null)
    }

    override fun handleError(e: Throwable, validationErrorsListener: BaseContract.Presenter.OnValidationErrorsListener?) {
        view.showProgressCircle(false)
        view.showLoader(false)
        if (e is RetrofitException) {
            if (!e.processNetworkError(view)) {
                val errorBodyString = e.response?.errorBody()?.string() ?: "{\"message\":\"Empty error body\"}"
                e.response?.errorBody()?.close()
                try {
                    val generalError = Gson().fromJson(errorBodyString, ApiInterface.GeneralError::class.java)
                    view.showError(generalError.message)
                } catch (e1: JsonSyntaxException) {
                    Log.i("BasePresenter", "Exception GeneralError: " + e1.message, e1)
                    try {
                        val errorsList = Gson().fromJson(errorBodyString, object : TypeToken<List<ApiInterface.ValidationErrors.ValidationError>>() {}.type) as List<ApiInterface.ValidationErrors.ValidationError>
                        validationErrorsListener?.onValidationErrors(ApiInterface.ValidationErrors().apply { setErrors(errorsList) })
                    } catch (e2: JsonSyntaxException) {
                        Log.e("BasePresenter", "Exception ValidationError: " + e2.message, e2)
                        view.showError("Some error occurred.\nPlease try later.")
                        try {
                            Crashlytics.log(Log.WARN, "Server Response Error", errorBodyString)
                        }catch (e: IllegalStateException){ /* CL not initialized, ignore */}
                    }
                }
            }

            if (e.response?.code() == 401) {
                logout()
            }
        } else {
            view.showError("Local error occurred.")
        }
    }


    override fun logout() {
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
            apiInterface.deleteFirebaseId(it.token)
                .subscribeOn(Schedulers.io())
                .subscribe(object : DisposableSingleObserver<Any>() {
                    override fun onSuccess(@NonNull any: Any) {

                    }

                    override fun onError(@NonNull e: Throwable) {
                        e.printStackTrace()
                    }
                })
        }

        session.setLoggedIn(false, 0)
        view.onLogout()
    }


    class MvpViewNotAttachedException : RuntimeException("Please call Presenter.subscribe(View) before requesting data to the Presenter")

}

package com.bornfight.common.mvp.ui

import android.app.Dialog
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Bundle
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bornfight.appname.App
import com.bornfight.appname.R
import com.bornfight.common.dagger.components.ActivityComponent
import com.bornfight.common.dagger.components.DaggerActivityComponent
import com.bornfight.common.dagger.modules.ActivityModule
import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.kotlin.visibleIf
import com.bornfight.common.mvp.BaseContract
import com.bornfight.common.session.DevicePreferences
import com.bornfight.common.session.SessionPrefImpl
import com.bornfight.utils.DialogUtil
import com.google.firebase.iid.FirebaseInstanceId
import com.greysonparrelli.permiso.Permiso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

/**
 * Created by tomislav on 25/02/16.
 * Updated to Kotlin on 04/06/18.
 */
abstract class BaseActivity : AppCompatActivity(), BaseContract.View {

    @Inject
    protected lateinit var session: SessionPrefImpl

    @Inject
    protected lateinit var devicePrefs: DevicePreferences

    @Inject
    protected lateinit var apiInterface: ApiInterface

    private lateinit var pd: Dialog

    private var toolbar: Toolbar? = null
    private var toolbarProgressCircle: ProgressBar? = null

    lateinit var notificationManager: NotificationManager
    lateinit var activityComponent: ActivityComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        //setLanguage();
        super.onCreate(savedInstanceState)
        Permiso.getInstance().setActivity(this)

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        activityComponent = DaggerActivityComponent.builder()
            .activityModule(ActivityModule(this))
            .applicationComponent((application as App).component)
            .build()
        activityComponent.inject(this)

        pd = DialogUtil.buildLoaderDialog(this, getString(R.string.please_wait))

        checkFirebaseSent()
    }

    override fun onResume() {
        super.onResume()
        Permiso.getInstance().setActivity(this)
    }

    private fun setLanguage() {
        val locale = Locale("hr", "HR")
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        applicationContext.resources.updateConfiguration(config, null)
    }

    override fun setContentView(@LayoutRes layoutResID: Int) {
        super.setContentView(layoutResID)

        toolbar = findViewById(R.id.toolbar)
        toolbarProgressCircle = findViewById(R.id.toolbarProgressCircle)

        toolbar?.let { setSupportActionBar(it) }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == android.R.id.home) {
            supportFinishAfterTransition()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    fun checkPermissionGranted(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
    }

    fun checkPermissionsGranted(vararg permissions: String): Boolean {
        for (permission in permissions) {
            if (!checkPermissionGranted(permission)) {
                return false
            }
        }
        return true
    }

    fun checkPermissionDenied(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_DENIED
    }

    /**
     * Check if a permission is denied with "Do not ask again" flag.
     * In that case, consider hiding the functionality which requires user location.
     */
    fun checkPermissionDeniedForGood(permission: String): Boolean {
        return (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_DENIED
            && !ActivityCompat.shouldShowRequestPermissionRationale(this, permission)
            && session.checkPermissionAsked(permission))
    }

    /*
        BaseView method implementations
     */
    override fun showError(errorMessage: String) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
    }

    override fun showShortInfo(info: String) {
        Toast.makeText(this, info, Toast.LENGTH_SHORT).show()
    }


    override fun showProgressCircle(show: Boolean) {
        toolbarProgressCircle?.visibleIf(show) ?: showLoader(show)
    }

    override fun showLoader(show: Boolean) {
        if (show) {
            pd.show()
        } else {
            pd.dismiss()
        }

        if (!show) {
            pd.setCancelable(true)
        }
    }

    override fun showLoader(show: Boolean, cancelable: Boolean) {
        pd.setCancelable(cancelable)
        showLoader(show)
    }

    override fun hideKeyboard() {
        val view = window.decorView
        if (view != null) {
            (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun onLogout() {
        // clear the flag for sending the Firebase token
        devicePrefs.isFirebaseTokenSent = false

        //TODO: open login page if logged out
        /*
        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK;
        startActivity(intent)
        supportFinishAfterTransition()
        */
    }

    private fun checkFirebaseSent() {
        if (session.isLoggedIn && !devicePrefs.isFirebaseTokenSent) {
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
                apiInterface.saveFirebaseId(it.token, "android")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : DisposableSingleObserver<Any>() {
                        override fun onSuccess(any: Any) {
                            devicePrefs.isFirebaseTokenSent = true
                        }

                        override fun onError(e: Throwable) {

                        }
                    })
            }
        }
    }

    override fun showError(stringResourceId: Int) {
        showError(getString(stringResourceId))
    }

    override fun showShortInfo(stringResourceId: Int) {
        showShortInfo(getString(stringResourceId))
    }

    override fun showInfoDialog(title: String?, description: String?, buttonText: String?) {
        DialogUtil.buildInfoDialog(
            this,
            title,
            description,
            buttonText
        ).show()
    }

    override fun showInfoDialog(@StringRes titleResourceId: Int, @StringRes descriptionResourceId: Int, @StringRes buttonText: Int) {
        showInfoDialog(
            if (titleResourceId != 0) getString(titleResourceId) else null,
            if (descriptionResourceId != 0) getString(descriptionResourceId) else null,
            if (buttonText != 0) getString(buttonText) else null
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Permiso.getInstance().setActivity(this)
        for (permission in permissions) {
            session.setPermissionAsked(permission)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Permiso.getInstance().setActivity(this)
    }

    companion object {

        init {
            //TODO enable if you must have full vector support on devices pre-Lollipop (vectors in Menu items etc.)
            // otherwise it is not recommended https://stackoverflow.com/a/41855830/2489759
            //AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }
    }
}

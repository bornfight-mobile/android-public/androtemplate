package com.bornfight.common.data.retrofit

import android.util.Log
import com.bornfight.appname.R
import com.bornfight.common.mvp.BaseContract
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException

/**
 * Created by tomislav on 09/01/2017.
 */

class RetrofitException internal constructor(message: String,
                                             /** The request URL which produced the error.  */
                                             val url: String?,
                                             /** Response object containing status code, headers, body, etc.  */
                                             val response: Response<*>?,
                                             /** The event kind which triggered this error.  */
                                             private val kind: Kind, exception: Throwable?,
                                             /** The Retrofit this request was executed on  */
                                             val retrofit: Retrofit?) : RuntimeException(message, exception) {

    fun processNetworkError(view: BaseContract.View): Boolean {
        if (kind == Kind.NETWORK) {
            view.showShortInfo(R.string.network_error)
            return true
        }
        return false
    }

    /**
     * HTTP response body converted to specified `type`. `null` if there is no
     * response or the class conversion failed.
     */
    @Throws(IOException::class)
    fun <T> getErrorBodyAs(type: Class<T>): T {
        if (response?.errorBody() == null) {
            throw IOException("Response error data is empty.")
        }
        try {
            val jsonObjectString = response.errorBody()?.string()
            var jsonObject = JSONObject()
            try {
                jsonObject = JSONObject(jsonObjectString)
            } catch (e: JSONException) {
                e.printStackTrace()
                try {
                    Log.d("array c", jsonObjectString)
                    val jsonArray = JSONArray(jsonObjectString)
                    jsonObject = jsonArray.getJSONObject(0)
                } catch (e1: JSONException) {
                    e1.printStackTrace()
                    try {
                        var errorMess: String? = jsonObjectString
                        if (errorMess == null || errorMess.isEmpty()) errorMess = response.message()
                        jsonObject.put("message", errorMess)
                    } catch (e2: JSONException) {
                        e2.printStackTrace()
                    }

                }

            }

            return Gson().fromJson(jsonObject.toString(), type)
            /*
            Converter<ResponseBody, T> converter = retrofit.responseBodyConverter(type, new Annotation[0]);
            return converter.convert(response.errorBody());
           */
        } finally {
            response.errorBody()?.close()
        }
    }

    /** Identifies the event kind which triggered a [RetrofitException].  */
    enum class Kind {
        /** An [IOException] occurred while communicating to the server.  */
        NETWORK,
        /** A non-200 HTTP status code was received from the server.  */
        HTTP,
        /**
         * An internal error occurred while attempting to execute a request. It is best practice to
         * re-throw this exception so your application crashes.
         */
        UNEXPECTED
    }

    companion object {

        fun httpError(url: String, response: Response<*>, retrofit: Retrofit): RetrofitException {
            val message = response.code().toString() + " " + response.message()
            return RetrofitException(message, url, response, Kind.HTTP, null, retrofit)
        }

        fun networkError(exception: IOException): RetrofitException {
            return RetrofitException(exception.message
                ?: "", null, null, Kind.NETWORK, exception, null)
        }

        fun unexpectedError(exception: Throwable): RetrofitException {
            return RetrofitException(exception.message
                ?: "", null, null, Kind.UNEXPECTED, exception, null)
        }
    }
}

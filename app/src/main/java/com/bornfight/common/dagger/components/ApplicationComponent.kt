package com.bornfight.common.dagger.components

import android.content.Context
import com.bornfight.common.dagger.modules.ApiServiceModule
import com.bornfight.common.dagger.modules.DataSourceModule
import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.session.DevicePreferences
import com.bornfight.common.session.SessionPrefImpl
import dagger.Component
import javax.inject.Singleton

/**
 * Created by ianic on 08/12/16.
 */

@Singleton
@Component(modules = [(ApiServiceModule::class), (DataSourceModule::class)])
interface ApplicationComponent {

    val context: Context

    val apiInterface: ApiInterface

    val sessionPrefImpl: SessionPrefImpl

    val devicePreferences: DevicePreferences

}
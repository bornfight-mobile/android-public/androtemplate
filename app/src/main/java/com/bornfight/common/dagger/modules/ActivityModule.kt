package com.bornfight.common.dagger.modules

import android.app.Activity
import android.content.Context

import com.bornfight.appname.main.MainContract
import com.bornfight.appname.main.MainPresenter
import com.bornfight.common.dagger.qualifiers.ActivityContext

import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by tomislav on 17/02/2017.
 */

@Module
class ActivityModule(private val mActivity: Activity) {

    @Provides
    @ActivityContext
    internal fun provideContext(): Context {
        return mActivity
    }

    @Provides
    internal fun provideActivity(): Activity {
        return mActivity
    }

    @Provides
    internal fun provideCompositeSubscription(): CompositeDisposable {
        return CompositeDisposable()
    }

    @Provides
    internal fun mainPresenter(presenter: MainPresenter): MainContract.Presenter<MainContract.View> {
        return presenter
    }
}

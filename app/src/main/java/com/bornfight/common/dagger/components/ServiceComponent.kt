package com.bornfight.common.dagger.components

import com.bornfight.appname.firebase.MyFirebaseMessagingService
import com.bornfight.common.dagger.scopes.ActivityScope
import com.bornfight.common.firebase.ProcessOpenNotificationService

import dagger.Component

/**
 * Created by tomislav on 16/03/2017.
 */

@ActivityScope
@Component(dependencies = [(ApplicationComponent::class)])
interface ServiceComponent {

    fun inject(myFirebaseMessagingService: MyFirebaseMessagingService)

    fun inject(processOpenNotificationService: ProcessOpenNotificationService)
}

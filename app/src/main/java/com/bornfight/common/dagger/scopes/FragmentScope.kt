package com.bornfight.common.dagger.scopes

import javax.inject.Scope

/**
 * Created by ianic on 08/12/16.
 */

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FragmentScope

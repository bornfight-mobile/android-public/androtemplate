package com.bornfight.common.dagger.components

import com.bornfight.appname.fragmentexample.ExampleFragment
import com.bornfight.common.dagger.modules.FragmentModule
import com.bornfight.common.dagger.scopes.FragmentScope

import dagger.Component

/**
 * Created by tomislav on 17/02/2017.
 */

@FragmentScope
@Component(dependencies = [(ApplicationComponent::class)], modules = [(FragmentModule::class)])
interface FragmentComponent {

    fun inject(cribFragment: ExampleFragment)
}

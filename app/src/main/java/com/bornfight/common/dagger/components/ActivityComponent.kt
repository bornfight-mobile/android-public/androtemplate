package com.bornfight.common.dagger.components

import com.bornfight.appname.main.MainActivity
import com.bornfight.common.dagger.modules.ActivityModule
import com.bornfight.common.dagger.scopes.ActivityScope
import com.bornfight.common.mvp.ui.BaseActivity

import dagger.Component

/**
 * Created by ianic on 26/01/17.
 */

@ActivityScope
@Component(dependencies = [(ApplicationComponent::class)], modules = [(ActivityModule::class)])
interface ActivityComponent {

    //activities
    fun inject(baseActivity: BaseActivity)

    fun inject(mainActivity: MainActivity)

}

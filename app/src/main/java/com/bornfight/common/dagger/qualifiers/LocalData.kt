package com.bornfight.common.dagger.qualifiers

import javax.inject.Qualifier

/**
 * Created by tomislav on 22/02/2017.
 */

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class LocalData

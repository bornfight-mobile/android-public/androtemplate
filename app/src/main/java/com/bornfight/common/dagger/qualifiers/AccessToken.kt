package com.bornfight.common.dagger.qualifiers

import javax.inject.Qualifier

/**
 * Created by ianic on 20/12/16.
 */

@Qualifier
annotation class AccessToken
